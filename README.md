# ADReSSo Challenge [IS 21 special session](https://edin.ac/3p1cyaI)

This directory contains the code for generation of the ADReSSo
dataset (directories `R/`, `util/`), data files generated with this code
(data/), and the files for the Challenge website (`website/`).

## Diagnosis task

This challenge encompasses AD/CN classifiaction and MMSE score
regression. 

### Generating a (maximal) balanced dataset

We employ the R script in 

- R/balanceds.R

to create a balanced dataset summary from the Pitt Cookie theft data
augmented with controls from Aphasia and Right Hemisphere Disorder
datasets (see details below). 

The script uses a propensity score approach to matching (Rosenbaum &
Rubin, 1983; Rubin 1973) implemented in the R package
[MatchIt](https://cran.r-project.org/web/packages/MatchIt/) (Ho et
al. 2007).  The function `getADReSSoDataset()` will read the CHATR
files in the directories specified in `makeVocalStatsDataset()` and
generate dataset matched according to propensity scores (defined in
terms of the probability of an instance of being treated as AD given
covariates age and gender, estimated through probit regression). 

Running:

```r
adrs <- getADReSSoDataset(ds =makeVocalStatsDataset(sprate = FALSE))
```

will generate a balanced dataset for all .cha files in
"../data/Pitt/Dementia/cookie" and "../data/Pitt/Control/cookie"
(assuming these are the directories where you keep those files. The
function returns a list `l` containing

- `adrs$ds`, a data frame containing the dataset as read by
  makeVocalStatsDataset() minus duplicated speakers (multiple visits;
  the algorithm picks the longest recording from a set of muiltiple
  visits for inclusion) plus new columns for base patient ID (sid) and
  visit number.

- `adrs$match.obj` the result of matching as described above. The
  function data.match(x$match.obj) returns the balanced dataset. See
  MatchIt package documentation for details.

A dataset so generated has been saved to `data\adressods.csv`, where
column `id` contains the file identifier (file name minus the
extension). The column `filename` contains the relative path to the
CHATR file to be used.


The figures below summarise the balanced mathed data:

```r
> summary(adrs$match.obj, interactions = T)

Call:
matchit(formula = dx ~ age + gender, data = ds[ds$dx %in% c("Control", 
    "ProbableAD"), ], method = "full", distance = "glm", link = "probit", 
    discard = "both")

Summary of Balance for All Data:
                   Means Treated Means Control Std. Mean Diff. Var. Ratio eCDF Mean eCDF Max
distance                  0.5822        0.4736          0.6552     1.2420    0.1570   0.2935
age                      70.6519       65.6083          0.6605     1.2809    0.1483   0.2935
gendermale                0.3407        0.3667         -0.0547          .    0.0259   0.0259
genderfemale              0.6593        0.6333          0.0547          .    0.0259   0.0259
age²                   5049.5556     4349.5917          0.6564     1.4656    0.1483   0.2935
age * gendermale         23.6222       24.1667         -0.0163     1.0711    0.0279   0.0787
age * genderfemale       47.0296       41.4417          0.1623     1.1533    0.1304   0.2324


Summary of Balance for Matched Data:
                   Means Treated Means Control Std. Mean Diff. Var. Ratio eCDF Mean eCDF Max Std. Pair Dist.
distance                  0.5565        0.5570         -0.0028     0.9864    0.0016   0.0328          0.0063
age                      69.3770       69.4139         -0.0048     0.9810    0.0013   0.0328          0.0051
gendermale                0.3525        0.3852         -0.0692          .    0.0328   0.0328          0.1633
genderfemale              0.6475        0.6148          0.0692          .    0.0328   0.0328          0.1633
age²                   4860.1803     4865.6270         -0.0051     0.9788    0.0013   0.0328          0.0050
age * gendermale         24.0984       26.7541         -0.0797     0.9237    0.0300   0.0437          0.1618
age * genderfemale       45.2787       42.6598          0.0760     0.9666    0.0301   0.0437          0.1559

Percent Balance Improvement:
                   Std. Mean Diff. Var. Ratio eCDF Mean eCDF Max
distance                      99.6       93.7      99.0     88.8
age                           99.3       92.3      99.1     88.8
gendermale                   -26.5          .     -26.5    -26.5
genderfemale                 -26.5          .     -26.5    -26.5
age²                          99.2       94.4      99.1     88.8
age * gendermale            -387.8      -15.5      -7.5     44.5
age * genderfemale            53.1       76.2      76.9     81.2

Sample Sizes:
              Control Treated
All            120.       135
Matched (ESS)   49.43     122
Matched        116.       122
Unmatched        0.         0
Discarded        4.        13
```

One can see that all standardized mean differences for the covariates
were well below 0.1 and all standardized mean differences for squares
and two-way interactions between covariates were well below .15,
indicating adequate balance for the covariates.  The propensity score
was estimated using a probit regression of the treatment on the
covariates `age` and `gender` (probit generated a better balanced than
logistic regression).

Balance for gender can be further tested by using the chisq test, for
instance:

```r
> cs <- droplevels(match.data(adrs$match.obj))[,c('dx', 'gender')] %>% table() %>% chisq.test()
> cs

	Pearson's Chi-squared test with Yates' continuity correction

data:  .
X-squared = 0, df = 1, p-value = 1

> cs$expected
            gender
dx               male   female
  Control    40.94118 75.05882
  ProbableAD 43.05882 78.94118
> cs$observed
            gender
dx           male female
  Control      41     75
  ProbableAD   43     79
```

The matched dataset looks like this:

```r
table(match.data(adrs$match.obj)$dx)

   Control        MCI     Memory PossibleAD ProbableAD   Vascular 
        91          0          0          0        115          0 
```

The age/gender matching is summarised by `plot(adrs$match.obj)`, which
shows the respective (empirical) quantile-quantile (qq) plots for the original and
balanced datasets. As usual, a qq plot showing instances near the
diagonal indicates good balance.

```r
## png(filename = '../ADReSSo-IS2021/eqqplot.png') ## to save as PNG 
plot(x$match.obj)
## dev.off()  ## write PNG
```

![eQQplot](eqqplot.png "eQQ Plot for ADReSSo dataset")

### Train/Test split

Train and test datasets are defined by random sampling once balancing
is done by function `trainTestSplit()`. By default this function
assignes 30% of instances to `test` and the remaining to `train`. 
Statistical summaries of the distributions of class, age and gender in
these sets can be obtained by calling `describeTrainTestSplit()`.

The descriptive stats of the split are the following:

```r
## do the split on matched data
> adsd  <- trainTestSplit(match.data(adrs$match.obj))
> describeTrainTestSplit(adsd)

Dx (test set) .
   Control ProbableAD 
      0.51       0.49 
Dx (train set) .
   Control ProbableAD 
      0.48       0.52 

Gender (test set) .
  male female 
  0.38   0.62 
Gender (train set) .
  male female 
  0.34   0.66

Age ranges$AD
        ageiv
gender   (45,50] (50,55] (55,60] (60,65] (65,70] (70,75] (75,80]
  male         0       1       7      10       6       8      11
  female       0       1       6      13      21      19      19

$CONTROL
        ageiv
gender   (45,50] (50,55] (55,60] (60,65] (65,70] (70,75] (75,80]
  male         0       1       6      12       9       9       3
  female       0       2      16      16      23      14       4

Age (full)
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
  53.00   63.00   68.00   67.77   73.00   80.00 
Age (test set)
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
   56.0    61.0    68.0    67.3    72.5    79.0 
Age (train set)
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
  53.00   64.00   68.00   67.97   73.00   80.00 
```

Once the dataset has been split, function `copyTranscripts(adrs)`
copies all `adrs$filename` to `data/test/transcripts/` or
`data/train/transcripts/` according to the split, renaiming each file
to its corresponding `adrs$adressfname`.

The structure of the data directory is

```
diagnosis/
├── test
│   ├── audio
│   └── transcripts
└── train
    ├── audio
    │   ├── ad
    │   └── cn
    └── transcripts
        ├── ad
        └── cn
```

The transcripts are *not* for distribution to ADReSSo challenge
participants. 


### The dataset as a CSV file, and CHA transcripts

The CSV was generated as follows:

```r
write.csv(file = '../data/adressods.csv', match.data(adrs$match.obj))
```

We will mostly be interested in the id, gender, age, dx, filename and
adressfname columns for the puspose of generating the audio files.

Transcripts and audio files from RHD and Olness controls were edited
so that only the cookie theft recordings and transcriptions were
left. The modified files have a suffix '-cookie' appended to the file
basename. E.g: the cookie section was exported from A-A/03.mp3 and
saved as 03-cookie.mp3. The data directory (see its structure
above) contains the .cha files and links audio files for the selected
balanced dataset. These were generated with

```r
copyTranscripts(adrs)
symlinkAudio(adrs)
```

### Files excluded or in need of improving
 
The files below had some problems, but only minga34a had to be
excluded.



| File     | DS     | Excluded? | Problem              | Fixed | New name  |
|----------|--------|-----------|----------------------|-------|-----------|
| minga34a | RHD    | Yes       | no Cookie Theft task | No    |           |
| A-A/03   | Olness | No        | no time stamps       | Yes   | 03-cookie |
| A-A/17   | Olness | No        | no time stamps       | yes   | 17-cookie |
| A-A/32   | Olness | No        | no time stamps       | yes   | 32-cookie |
| Cauc/02  | Olness | No        | no time stamps       | yes   | 02-cookie |
| Cauc/08  | Olness | No        | no time stamps       | yes   | 08-cookie |
| Cauc/17  | Olness | No        | no time stamps       | yes   | 17-cookie |


## Disease progression task

This task uses longitudinal data spanning baseline and year-2 data
collection visits. It involves classifying patients into
'decline'/'no-decline' categories, given fluency-test speech collected
at baseline. Decline is defined as a difference in MMSE score between
baseline and year-2 greater than or equal 5 points
(i.e. mmse(baseline) - mmse(y2) >= 5).

### Generating the progression dataset

Run:

```r
## get dataset based on metadata spreadsheet and cha files
z  <-  makeMMSEprogDataset(to='mmse2')
## Select the points that have audio
z1 <- z[z$audioexist,]
## exclude thoso files that have participants in the test set of the Dx task
z2 <- z1[ z1$id %in% adsd$sid[adsd$test],]
## select safe instances (those not in the AD detection test set), to avoid leaking information
z1 <- z1[ z1$id %notin% adsd$sid[adsd$test],]
## split z1 data to a 7:3 ratio 
z1 <- trainTestSplit(z1,test.prob = .3)
## split z2 dataset (at 20%, to compensate for the excluded overlapping files)
z2 <- trainTestSplit(z2,test.prob = .2)
## bind excluded files back into z2
z2 <- rbind(z1, z2)
```

The dataset has the following characteristics:

```r
describeTrainTestSplit(z2, target='decline')
Split: Test (T or F).
FALSE  TRUE 
   73    32 
.
FALSE  TRUE 
  0.7   0.3 
Dx (test set) .
       MCI PossibleAD ProbableAD 
      0.06       0.06       0.88 
Dx (train set) .
       MCI PossibleAD ProbableAD        SML   Vascular 
      0.14       0.11       0.70       0.01       0.04 
Decline (test set) .
FALSE  TRUE 
 0.69  0.31 
Decline (train set) .
FALSE  TRUE 
 0.79  0.21 
Gender (test set) .
female   male 
  0.56   0.44 
Gender (train set) .
female   male 
  0.59   0.41 
Age ranges$TARGET
        ageiv
gender   (45,50] (50,55] (55,60] (60,65] (65,70] (70,75] (75,80]
  female       1       0       2       3       7       2       2
  male         0       0       1       0       0       2       3

$CONTROL
        ageiv
gender   (45,50] (50,55] (55,60] (60,65] (65,70] (70,75] (75,80]
  female       0       0       5       9       5       8       9
  male         1       1       4       6       8      10       7

Age (full)
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
  49.00   64.00   70.00   70.16   77.00   88.00 
Age (test set)
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
  53.00   63.00   70.00   68.66   73.50   87.00 
Age (train set)
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
  49.00   65.00   71.00   70.82   77.00   88.00 
```

Once these sets have been generated, all that remains is to copy/link
the files to their respective folders. This can be done as follows:

```r
symlinkAudio(z2, target='decline')
copyTranscripts(z2, target='decline')
```

The files will be placed in this directory structure:

```
progression/
├── test
│   ├── audio
│   └── transcripts
└── train
    ├── audio
    │   ├── decline
    │   └── no_decline
    └── transcripts
        ├── decline
        └── no_decline
```


The metadata has been saved to `../data/adresspds.csv' via:

```r
write.csv(file = '../data/adresspds.csv', z2)
```

### Obscuring (ramdomisation) of test file names

In order to prevent "guessing" of categories based on file names, as
they are sequentially assigned by the procedure above and the
categories are not uniformly distribute wrt file names. This is
accomplished essentially by linking the original files in
`.../test/{audio,segmentation}` to new files with ramdomised names
into the corresponding files in `.../test-dist/`.  This is
accomplished by the `r linkRandomizeTestSetNames()` function. 

Files constaining mappings of the original names to the obscured
names are saved in `{diagnosis,progression}/test/dist-id-mapping.csv`. 

A file containing the original `adresspds.csv` and an extra column for
the randomised file names has been generated as follows:

`r
y <- read.csv('../data/adresspds.csv')
z <- read.csv('../data/progression/test/id_mapping_and_labels.csv')
## left join
w <- merge(y, z, by.x='adressfname', by.y='ADRSoID', all.x=T)
w <- w[,c(1:20,22)]
names(w)[21] <- 'randfname'
write.csv(file = '../data/adresspds-withmapping.csv', w)
`


## Packaging the datasets for distribution

A skeleton of the ADReSSo dataset distribution is available in the
following directory
[data/distrib/ADReSSo21/](data/distrib/ADReSSo21/). Packaging of test
and training sets is handled by the
[Makefile](data/distrib/ADReSSo21/Makefile) in this directory. Running 

```sh
make diagnosis
make progression
make diagnosis-test
make progression-test
```

will create the tar archives for the training and test sets
respectively, and store them in the directory immediately above
`ADReSSo21`.

## Generating ASR transcripts, morphology ans syntax based features

ASR transcripts were generated from the enhanced audio files through
the Google ASR API. The JSON output was then converted into CHAT files
by [util/googleasr2chat.py](util/googleasr2chat.py):

```sh
## convert Fasih's .txt files 
for f in *.txt ; do mv $f $(echo "$f" | sed s/-trans.txt//)-asr.json     ; done 
## conver all .json to .cha
for f in *.json ; do googleasr2chat.py $f   ; done 
```

The ASR transcripts are stored in the relevantly named directories:

```
   ├── test
   │   ├── asr
   │   └── ....
   └── train
       ├── asr
       │   ├── ad
       │   └── cn
       |   ...
       └── ...
```

MOR/GRASP analyzers from the [CLAN
software](https://dali.talkbank.org/clan/) were used to add `%mor` and
`%gram` layers to the CHAT files. 


## Required software

### For dataset balancing (balanceds.R):

- [R](https://cran.r-project.org/)
- [MatchIt](https://cran.r-project.org/package=MatchIt) 
- [magrittr](https://cran.r-project.org/package=magrittr)

### For extraction of manual segmentation

- [Vocaldia](https://cran.r-project.org/package=vocaldia) 

### For ASR transcription and linguistic analysis

- Google ASR API
- python
- [util/googleasr2chat.py](util/googleasr2chat.py)
- [CLAN](https://dali.talkbank.org/clan/)

## Base datasets

- Pitt Cookie Theft (Control/Dementia)
  <https://dementia.talkbank.org/access/English/Pitt.html>
  
- Olness Aphasia / Cookie Theft (Control/{A-A,Cauc})
  <https://aphasia.talkbank.org/access/English/Other/NonProtocol/Olness.html>
  
- Right Hemisphere Disorder (RHD) Cookie Theft
  (Control/{Minga,Nazareth})
  <https://rhd.talkbank.org/access/English/Control.html>

- ChialFlahive Aphasia Corpus, Cookie Theft (REN/cookie)
  <https://aphasia.talkbank.org/access/English/Other/NonProtocol/ChialFlahive.html>


## Refs

Rosenbaum, Paul R., and Donald B. Rubin. 1983. “The Central Role of
the Propensity Score in Observational Studies for Causal Effects.”
Biometrika 70 (1): 41–55. <https://doi.org/10.1093/biomet/70.1.41>.

Rubin, Donald B. 1973. “Matching to Remove Bias in Observational
Studies.” Biometrics 29 (1): 159. <https://doi.org/10.2307/2529684>.

Ho, Daniel E., Kosuke Imai, Gary King, and Elizabeth
A. Stuart. 2007. “Matching as Nonparametric Preprocessing for Reducing
Model Dependence in Parametric Causal Inference.” Political Analysis
15 (3): 199–236. https://doi.org/10.1093/pan/mpl013. 

Luz, S. 2013. Automatic identification of experts and performance
prediction in the multimodal math data corpus through analysis of
speech interaction. In Proceedings of the 15th ACM on International
conference on multimodal interaction (ICMI '13). Association for
Computing Machinery, New York, NY, USA,
575–582. DOI:https://doi.org/10.1145/2522848.2533788


