#!/usr/bin/perl
# 
# getasrstats.PL --- extract WER stats from NIST sclite SUM style output (-o sum)
# output

# Author:  <s.luz@ed.ac.uk>
# Created: 22 Mar 2021
# $Revision$
# Keywords:


#     Permission  to  use,  copy, and distribute is hereby granted,
#     providing that the above copyright notice and this permission
#     appear in all copies and in supporting documentation.

# Commentary:

# Change log:
#

# Code:
my $rcsid =   "\$Id:  $>";
my $version = substr("\$Revision$>",11,-length($0));

BEGIN{ 
    $0 =~ /(.*)\/[^\/]/;
    $pgd = $1;
    push (@INC,("./",
	       "$pgd/",
	       "$pgd/Lib/"
		));
		$me   = $0;
		$me =~ s/.*\///;
}
use Getopt::Std;


sub usage {
    die<<"END";

  $me version $version
  extract WER stats from NIST sclite SUM style output (-o sum) output

  Options:          
      -h	  Help   just display this message and quit.
      -f  files   glob pattern for all files [Default: '*.sys']
      -o  fname   file to save the extracted stats in [Default: asr-stats.csv]

END
}

getopts('hf:');
usage
    if ( $opt_h );

my $ls = $opt_f || '*.sys';
my $of = $opt_o || 'asr-stats.csv';

my @files = glob($ls);

my $content = "NoSents\tNoWords\tCorr\tSub\tDel\tIns\tErr\tS.Err\n";


foreach (@files){    
    my $file = $_;
    print STDERR "Extracting from $file\n";
    open(FI, $file) or die ("error opening $file: $1");
    while(<FI>)
    {
        if ( /.*Sum\/Avg.+?(\d+).+?(\d+).+?(\d+).+?(\d+).+?(\d+).+?(\d+).+?(\d+).+?(\d+)/ ) {
            $content .= "$1\t$2\t$3\t$4\t$5\t$6\t$7\t$8\n"
        }
    }
    close FI;
}
    
open(FO, ">$of") or die ("error opening $of: $1");



print FO $content;

close FO;


# end 
