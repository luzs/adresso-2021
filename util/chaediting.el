;;; CHAEDITING.EL --- Edit CHATR files

;; Copyright (C) 2021 luzs

;; Author: luzs luzsa@stronzi.org
;; Maintainer: luzs luzsa@stronzi.org
;; Created: 31 Jan 2021
;; Version: 1.0
;; Keywords:

 
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 1, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; A copy of the GNU General Public License can be obtained from this
;; program's author (send electronic mail to luzsa@stronzi.org) or
;; from the Free Software Foundation, Inc., 675 Mass Ave, Cambridge,
;; MA 02139, USA.

;; LCD Archive Entry:
;; chaediting|luzs|luzsa@stronzi.org
;; |1
;; |$Date$|$Revision$|~/packages/%f

;;; Commentary:

;;; Change log:
;; $Log$

;;; Code:

(defconst %b-version (substring "$Revision$" 11 -2)
  "$Id$

Report bugs to: %U %a")


;; replace all sentence timestamps by timestamps xhifted by offset (e.g.
;; with offset 795232
;; *INV:	I'm going to show you some pictures okay? 797959_807503
;; gets replaced by
;; *INV:	I'm going to show you some pictures okay? 2727_12271
;;
;; this can also be done interactively by doing "Search->Regexp Forward..."
;; entering search/replacement:
;; \([0-9]+\)_\([0-9]+\) → \,(format "%s_%s" (- (string-to-number \1) 795232) (- (string-to-number \2) 795232))
;;
(defun cha-shift-times (offset)
  (interactive "nEnter time offset (in milisenconds): ")
  (save-excursion
    (while (re-search-forward "\\([0-9]+\\)_\\([0-9]+\\)" nil t)
      (replace-match (format "%s_%s"
                             (number-to-string
                              (- (string-to-number
                                  (buffer-substring (match-beginning 1) (match-end 1)))
                                 offset))
                             (number-to-string
                              (- (string-to-number
                                  (buffer-substring (match-beginning 2) (match-end 2)))
                                 offset))
                     ))
    )))


;;; %F ends here
