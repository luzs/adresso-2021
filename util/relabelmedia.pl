#!/usr/bin/perl
# 
# relabelmedia.pl --- change @MEDIA labels to match file name

# Author:  <s.luz@ed.ac.uk>
# Created: 22 Jan 2020
# $Revision$
# Keywords:


#     Permission  to  use,  copy, and distribute is hereby granted,
#     providing that the above copyright notice and this permission
#     appear in all copies and in supporting documentation.

# Commentary:

# Change log:
#

# Code:
my $rcsid =   "\$Id:  $>";
my $version = substr("\$Revision$>",11,-length($0));

BEGIN{ 
    $0 =~ /(.*)\/[^\/]/;
    $pgd = $1;
    push (@INC,("./",
	       "$pgd/",
	       "$pgd/Lib/"
		));
		$me   = $0;
		$me =~ s/.*\///;
}
use Getopt::Std;


sub usage {
    die<<"END";

  $me version $version
  Change @MEDIA labels to match file name

  usage: $me  [-h] -f chafile 

  Options:          
      -h	  Help     just display this message and quit.
      -f          chafile  cha file for replacing media ID in
END
}

getopts('hf:');
usage
    if ( $opt_h );
my $infname = "$opt_f.old";
rename($opt_f, $infname);

my $media = $1
    if $opt_f =~ /(.+)\..+/;

my $content = '';
open(FI, $infname) or die ("error opening $infname: $1");
while(<FI>)
{
    print STDOUT "$2 --> $media\n"
        if s/(\@Media:[\t ]+)(.+?),/$1$media,/;
    $content .= $_;
    }
close FI;

open(FO, ">$opt_f") or die ("error opening $opt_f: $1");

print FO $content;

close FO;


# end 
