#!/usr/bin/env python
# -*- coding: utf-8 -*-
## Convert Google ASR JSON files into basic CHAT files 
## (c) S Luz <luzs@acm.org>
import json
import sys
import os
import re

cha_header_template = '''@UTF8
@PID:	{}
@Begin
@Languages:	eng
@Participants:	PAR Participant, INV Investigator
@ID:	eng|UPMC|PAR|||||Participant|||
@ID:	eng|UPMC|INV|||||Investigator|||
@Media:	{}.wav, audio
@Comment: No diarisation, so PAR and INV utterances collapsed into PAR
'''

def main():
    if len(sys.argv) < 2:
        sys.exit("Usage:\n  googleasr2chat google-asr-file.json [extension of CHA file]\n")
    if len(sys.argv) < 3:
        outext = 'cha'
    else:
        outext = sys.argv[2]
    json2chat(sys.argv[1],outext)

def json2chat(jsonlfname,  outext):
    base, extension = os.path.splitext(jsonlfname)
    outfile = "{}.{}".format(base, outext)
    print("Converting file {} -> {}".format(jsonlfname, outfile))
    jsonl = open(jsonlfname, encoding='utf-8')
    chahandle = open(outfile,'w', encoding='utf-8')
    writechaheader(chahandle,re.sub('-.*$', '', base))
    i = 0
    jsond = json.load(jsonl)
    if 'response' in jsond and 'results' in jsond['response']:
        for uttl in jsond['response']['results']:
            ut = uttl['alternatives'][0]['transcript']
            ws = uttl['alternatives'][0]['words']
            st = ws[0]['startTime']
            et = ws[len(ws)-1]['endTime']
            #print("{}--{}--{}--".format(ut, st, et))
            writechautterance(chahandle, 'PAR', ut, st, et)
    else:
        print('WARNING: Empty ASR transcript; writing empty CHAT file: ', outfile)
    jsonl.close()
    writechaend(chahandle)
    chahandle.close

def writechaheader(handle, media):
    handle.write(cha_header_template.format(media, media)) 

def writechautterance(handle, spkr, utterance, stime, etime):
    stime = float(re.sub('s', '', stime))*1000 
    etime = float(re.sub('s', '', etime))*1000
    utterance = re.sub('^\s+|\W+$', '', utterance)
    handle.write("*{}:	{} . {:.0f}_{:.0f}\n".format(spkr, utterance.lower(), stime, etime))

def writechaend(handle):
    handle.write("@End\n") 

    
if __name__ == "__main__":
   main()




