# Evaluation of ASR results

## Converting Google ASR JSON files to CHAT format

ASR transcripts were generated from the enhanced audio files through
the Google ASR API. The JSON output was then converted into CHAT files
by `../util/googleasr2chat.py`:

```sh
## convert Fasih's .txt files 
for f in *.txt ; do mv $f $(echo "$f" | sed s/-trans.txt//)-asr.json     ; done 
## conver all .json to .cha
for f in *.json ; do googleasr2chat.py $f   ; done 
```

The ASR transcripts are stored in the relevantly named directories:

```
../data
   ├── test
   │   ├── asr
   │   └── ....
   └── train
       ├── asr
       │   ├── ad
       │   └── cn
       |   ...
       └── ...
```

## Creating evaluation directories

The files for evaluation are stored in the [data/](data/)
directory. They consis of links to the relevant data files under
[../data/](../data/). For instance:

```
data/diagnosis/adrso316-asr.cha -> ../../../data/diagnosis/train/asr/cn/adrso316-asr.cha
```

The links were created from bash as follows:

```sh
cd data/diagnosis/
for f in ../../../data/diagnosis/train/asr/cn/*-asr.cha ; do ln -s $f ; done
for f in ../../../data/diagnosis/train/trascripts/cn/*.cha ; do ln -s $f ; done
for f in ../../../data/diagnosis/train/asr/ad/*-asr.cha ; do ln -s $f ; done
for f in ../../../data/diagnosis/train/trascripts/ad/*.cha ; do ln -s $f ; done
for f in ../../../data/diagnosis/test/asr/*-asr.cha ; do ln -s $f ; done
for f in ../../../data/diagnosis/test/trascripts/*.cha ; do ln -s $f ; done
```

## Generating sclite's input files

NIST's sclite needs files in STM (reference) and TXT (hypothesis)
formats to align the transcripts and calculate WER etc.

Those files were generated as follows:

```sh
# generating reference (manual transcript) files
for f in *[0-9].cha ; do perl ~/lib/projects/DTP/HRI-Dementia/ADReSSo-IS2021/util/chat2stm.pl -f $f ; done
# generating hypothesis (ASR) files
for f in *-asr.cha ; do perl ~/lib/projects/DTP/HRI-Dementia/ADReSSo-IS2021/util/chat2stm.pl -t -f $f ; done
```

## Computing WER and collecting the statistics for all files

To compute WER for each file, do:

```sh
for f in *-asr.txt ; do  ~/lib/c/NIST-SCTK.git/src/sclite/sclite -r `echo $f|sed s/-asr.txt/.stm/` stm -h $f txt -o sum ; done
```

Now, gather the stats under a single CSV (actually TSV) file:

```sh
perl ~/lib/projects/DTP/HRI-Dementia/ADReSSo-IS2021/util/getasrstats.pl
```

A file named asr-stats.csv will be generated. It looks like this:

```
NoSents NoWords Corr    Sub     Del     Ins     Err     S.Err
18      173     82      7       7       5       9       8
6       86      57      0       9       3       33      7
4       43      67      4       7       0       25      6
18      144     34      7       5       6       59      7
9       92      22      8       3       3       73      9
8       63      47      6       3       2       49      2
```


NB: If you wish to see the details of the alignments, use: 

```sh
for f in *-asr.txt ; do  ~/lib/c/NIST-SCTK.git/src/sclite/sclite -r `echo $f|sed s/-asr.txt/.stm/` stm -h $f txt -o pralign ; done 
```

## Results

### Diagnosis tasks datasets:

Reading `asr-stats.csv` in R produces a summary of stats:

```r
a <- read.csv('data/diagnosis/asr-stats.csv', sep = '\t' )
stem(a$Err)
#  The decimal point is 1 digit(s) to the right of the |
#
#  0 | 03
#  0 | 99
#  1 | 14
#  1 | 55578999
#  2 | 2222334
#  2 | 58
#  3 | 002233
#  3 | 557888999
#  4 | 11223334
#  4 | 55666666677778889999
#  5 | 0000111223333444
#  5 | 5555666777777788888888899999999
#  6 | 000022333334444
#  6 | 556666778888888899999999
#  7 | 0000001112223333333344
#  7 | 55566677778888999
#  8 | 000001111122222333444
#  8 | 566777788999
#  9 | 00112344
#  9 | 66666

summary(a)

#   NoSents          NoWords           Corr            Sub       
# Min.   :  4.00   Min.   : 21.0   Min.   : 1.00   Min.   :0.000  
# 1st Qu.: 13.00   1st Qu.: 86.0   1st Qu.:17.00   1st Qu.:2.000  
# Median : 17.00   Median :116.0   Median :29.00   Median :4.000  
# Mean   : 18.38   Mean   :128.1   Mean   :31.02   Mean   :4.405  
# 3rd Qu.: 22.00   3rd Qu.:152.0   3rd Qu.:41.00   3rd Qu.:7.000  
# Max.   :120.00   Max.   :506.0   Max.   :92.00   Max.   :9.000  
#      Del              Ins             Err            S.Err      
# Min.   : 0.000   Min.   :0.000   Min.   : 0.00   Min.   :0.000  
# 1st Qu.: 4.000   1st Qu.:2.000   1st Qu.:48.00   1st Qu.:2.000  
# Median : 7.000   Median :5.000   Median :62.00   Median :4.000  
# Mean   : 7.523   Mean   :4.734   Mean   :60.09   Mean   :4.549  
# 3rd Qu.:10.000   3rd Qu.:7.000   3rd Qu.:76.00   3rd Qu.:7.000  
# Max.   :41.000   Max.   :9.000   Max.   :96.00   Max.   :9.000  
``` 

We can see that the WER is quite high on average (62) ranging widely
from very good to very poor transcrition.


## Software used:

- ASR software (Google ASR API used in this case)

- [util/googleasr2chat.py](../util/googleasr2chat.py): Convert Google
  ASR JSON files into basic CHAT files

- [util/chat2stm.pl](../util/chat2stm.pl): convert CHAT file to
  segment time mark (STM) format used by NIST's sclite tool which
  scores speech recognition system output

- [util/getasrstats.pl](../util/getasrstats.pl): extract WER stats
  from NIST sclite SUM style output (-o sum)
  
- NIST's [SCTK](https://github.com/usnistgov/SCTK): NIST Scoring
  Toolkit 

- GNU bash 
