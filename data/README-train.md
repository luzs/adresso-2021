# This directory contains the training data for the [IS2021 ADReSSo Challenge](https://edin.ac/3p1cyaI).

The data are in the following directories structure and files:

```
  ├── diagnosis
  │   └── train
  │       ├── audio
  │           ├── ad
  │           └── cn
  └── progression
    └── train
        ├── audio
            ├── decline
            └── no_decline
```

he `diagnosis` directory contain the data for the diagnosis and MMSE
score prediction tasks, while `progression` contains the disease
progression (cognitive decline) prediction task.

## Contents

TBD (Fasih)
