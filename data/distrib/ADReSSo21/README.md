# [ADReSSo IS-2021](https://edin.ac/3p1cyaI) data 

This directory contains the full ADReSSo dataset, in the following
hierarchy:

```
.
├── diagnosis
│   ├── test
│   │   ├── audio
│   │   └── transcripts
│   └── train
│       ├── audio
│       │   ├── ad
│       │   └── cn
│       └── transcripts
│           ├── ad
│           └── cn
└── progression
    ├── test
    │   ├── audio
    │   └── transcripts
    └── train
        ├── audio
        │   ├── decline
        │   └── no_decline
        └── transcripts
            ├── decline
            └── no_decline
```

The `diagnosis` directory contain the data for the diagnosis and MMSE
score prediction tasks, while `progression` contains the disease
progression (cognitive decline) prediction task.

This directory also contains files 

- `README-train.md`: with a description of the training set, to be
  released together with subtrees:

```
  ├── diagnosis
  │   └── train
  │       ├── audio
  │           ├── ad
  │           └── cn
  └── progression
    └── train
        ├── audio
            ├── decline
            └── no_decline
```

- `README-test.md`: with a description of the tes set (to be
  withheld). This refers to subtrees:
  
  ```
  ├── diagnosis
  │   ├── test
  │       ├── audio
  │       └── transcripts
  └── progression
      ├── test
          ├── audio
          └── transcripts
```
  
The sub-task directory contain specific README.md files for
distribution.

On a Unix shell:

```sh
make diagnosis
make progression
```

creates archive to be distributed to participants of the diagnosis
and progression sub-tasks, named ADReSSo21-diagnosis.tgz and 
ADReSSo21-progression.tgz 

Running:

```sh
make
```

creates an archive containing all (training and test) data.


