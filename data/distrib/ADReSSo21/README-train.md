# This directory contains the training data for the [IS2021 ADReSSo Challenge](https://edin.ac/3p1cyaI).

The data are in the following directories structure and files:

```
  ├── diagnosis
  │   └── train
  │       ├── audio
  │           ├── ad
  │           └── cn
  └── progression
    └── train
        ├── audio
            ├── decline
            └── no_decline
```

the `diagnosis` directory contain the data for the diagnosis and MMSE
score prediction tasks, while `progression` contains the disease
progression (cognitive decline) prediction task.

## Contents

The directories contain the enhanced, volume normalised audio data for
the diagnosis and MMSE score prediction tasks.

Also included are the utterance segmentation files (diarisation), in
CSV format. These files are for those who choose to do the segmented
prediction sub-task. The segmented prediction and speech-only
sub-tasks will be assessed separately.
