# This directory contains the test data for the [ADReSS Challenge](https://edin.ac/3p1cyaI).

The data are in the following directories structure and files:

  ```
  ├── diagnosis
  │   ├── test
  │       └── audio
  │
  └── progression
      ├── test
          └── audio

```


## Contents

The test files.
